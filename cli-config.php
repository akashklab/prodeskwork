<?php

define("ROOT_PATH", dirname(__FILE__), true);

require_once ROOT_PATH."/models/entities_manager.php";

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);