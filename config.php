<?php
  define("ROOT_PATH", dirname(__FILE__), true);
//var_dump(root_path);
  $file_content = file_get_contents(ROOT_PATH . "/configuration.json", FILE_USE_INCLUDE_PATH);
//var_dump($file_content);
  $settings = json_decode($file_content);

  if ($settings->{"env"} == "production") {
    define("DEVELOPMENT_MODE", false, true);
  } else {
    define("DEVELOPMENT_MODE", true, true);
  }

  define("DATABASE_HOST", $settings->{"host"});
  define("DATABASE_NAME", $settings->{'database'});
  define("DATABASE_USERNAME", $settings->{'username'});
  define("DATABASE_PASSWORD", $settings->{'password'});

  function logger($message)
  {
    error_log("LOGGER: $message\n", 3, ROOT_PATH . "/logs/logger.log");
  }

  function error_logger($message)
  {
    error_log("ERROR: $message\n", 3, ROOT_PATH . "/logs/error_logger.log");
  }

  define("ENTITIES_MANAGER_PATH", ROOT_PATH . "/models/entities_manager.php");

  if (DEVELOPMENT_MODE === false) {
    require_once 'admin_script';
  }

  function shutdown()
  {
    $error = error_get_last();
    //check if it's a core/fatal error, otherwise it's a normal shutdown
    if ($error !== NULL && $error['type'] === E_ERROR) {
      error_logger($error['message']);
      error_logger("TYPE:" . $error['type']);
      error_logger("FILE:" . $error['file']);
      error_logger("LINE:" . $error['line']);
      error_logger("************************************************************************");
      if (DEVELOPMENT_MODE === false):
        http_response_code(500);
        die();
      else:
        http_response_code(500);
        die();
      endif;
    }
  }

  register_shutdown_function('shutdown');