<?php
  
  require_once ROOT_PATH . '/helpers/general_func.php';
  
  function is_null_or_empty_string($question)
  {
    return (!isset($question) || trim($question) === '');
  }
  
  function is_valid_email($email)
  {
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
  }
  
  function safe_echo($var)
  {
    if (isset($var)) {
      return $var;
    } else {
      return '';
    }
  }
  
  function length_is_less_than($var, $size)
  {
    return strlen($var) < $size;
  }
  
  function length_is_greater_than($var, $size)
  {
    return strlen($var) > $size;
  }
  
  function has_special_chars($var)
  {
    if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $var)) {
      return true;
    } else {
      return false;
    }
  }
  
  function lower_and_trim($var)
  {
    return strtolower(trim($var));
  }
  
  function is_valid_ip_address($ip_address)
  {
    return filter_var($ip_address, FILTER_VALIDATE_IP) ? true : false;
  }
  
  function is_valid_url($url)
  {
    return filter_var($url, FILTER_VALIDATE_URL) ? true : false;
  }
  
  function is_valid_country($country)
  {
    return isset($countries[ $country ]);
  }
  
  function validation_check($data, $validations, $errors, $base)
  {
    foreach ($validations as $key => $value):
      
      if ($key == 'NULL_OR_EMPTY'):
        if (is_null_or_empty_string($data)):
          array_push($errors, "$base can't be blank");
          return $errors;
        endif;
      endif;
      
      if ($key == 'VALID_EMAIL'):
        if (!is_valid_email($data)):
          array_push($errors, "$base is not valid email");
        endif;
      endif;
      
      if ($key == 'MAX_CHAR'):
        if (length_is_greater_than($data, $value)):
          array_push($errors, "$base can't be more than $value characters");
        endif;
      endif;
      
      if ($key == 'MIN_CHAR'):
        if (length_is_less_than($data, $value)):
          array_push($errors, "$base require minimum $value characters");
        endif;
      endif;
      
      if ($key == 'ONLY_CHAR'):
        if (!preg_match('/^[A-Za-z\s]+$/', $data)):
          array_push($errors, "$base can be only characters");
        endif;
      endif;
      
      if ($key == 'URL'):
        if (!is_valid_url($data)):
          array_push($errors, "$base can be only characters");
        endif;
      endif;
      
      if ($key == 'COUNTRY'):
        if (!is_valid_country($data)):
          array_push($errors, "$base is not correct");
        endif;
      endif;
    
    endforeach;
    
    return $errors;
  }