<?php
  function createProfile($user, $em)
  {
    if ($user->getIsClient()):
      $klazz = new Client;
      $klazz->setUserId($user->getId());
      $em->persist($klazz);
      $em->flush();
    else:
      $klazz = new Professional;
      $klazz->setUserId($user->getId());
      $em->persist($klazz);
      $em->flush();
    endif;
  }