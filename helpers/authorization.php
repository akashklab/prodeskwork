<?php
  
  require_once ROOT_PATH . '/helpers/general_func.php';
  require_once ENTITIES_MANAGER_PATH;
  require_once ROOT_PATH . '/helpers/require_all_helper.php';

## Current Logged in User
  $current_user = isset($_SESSION[ "user_id" ]) ? $em->getRepository('User')->findOneBy([ 'id' => $_SESSION[ "user_id" ] ]) : null;
  
  function authentication($current_user)
  {
    if (!isset($current_user)):
      header('Location:/login.html');
      die();
    endif;
  }
  
  if (isset($current_user)):
    define("LOGGED_IN", true);
    if ($current_user->getIsClient()):
      if ($current_user->getIsIndividual()):
        define("I_AM_A_CLIENT", true);
        define("I_AM_A_C_CLIENT", false);
      else:
        define("I_AM_A_CLIENT", false);
        define("I_AM_A_C_CLIENT", true);
      endif;
      
      define("I_AM_A_PRO", false);
      define("I_AM_A_C_PRO", false);
    else:
      if ($current_user->getIsIndividual()):
        define("I_AM_A_PRO", true);
        define("I_AM_A_C_PRO", false);
      else:
        define("I_AM_A_PRO", false);
        define("I_AM_A_C_PRO", true);
      endif;
      
      define("I_AM_A_CLIENT", false);
      define("I_AM_A_C_CLIENT", false);
    endif;
  else:
    define("LOGGED_IN", false);
    define("I_AM_A_PRO", false);
    define("I_AM_A_C_PRO", false);
    define("I_AM_A_CLIENT", false);
    define("I_AM_A_C_CLIENT", false);
  endif;
  
  if (isset($current_user)):
    $profile_class = $current_user->getIsClient() === true ? "Client" : "Professional";
    
    ## Current Logged in User 's Profile
    $profile = $em->getRepository($profile_class)->findOneBy([ 'user_id' => $current_user->getId() ]);
    
    ## Condition for old record
    if ($profile == null):
      createProfile($current_user, $em);
      $profile = $em->getRepository($profile_class)->findOneBy([ 'user_id' => $current_user->getId() ]);
    endif;
  else:
    $profile = null;
  endif;