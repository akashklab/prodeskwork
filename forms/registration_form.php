<?php

  require_once ROOT_PATH . '/helpers/general_func.php';
  require_once ENTITIES_MANAGER_PATH;
  require_once ROOT_PATH . '/helpers/require_all_helper.php';

  ## Pro Registration

  if (isset($_POST['register-pro-form-submit'])):

    $errors         = [];
    $is_client      = $_POST['is-client'];
    $pro_full_name  = $_POST['full-name'];
    $pro_email      = $_POST['email'];
    $is_individual  = $_POST['is-individual'];
    $pro_password   = $_POST['password'];
    $pro_repassword = $_POST['repassword'];

    $errors = validation_check($pro_full_name,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 3,
       "MAX_CHAR"      => 50,
       "ONLY_CHAR"     => true],
      $errors, "Full Name or Company Name");

    $errors = validation_check($pro_email,
      ["NULL_OR_EMPTY" => false,
       "MAX_CHAR"      => 50,
       "VALID_EMAIL"   => true],
      $errors, "Email");

    $errors = validation_check($pro_password,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 5,
       "MAX_CHAR"      => 50],
      $errors, "Password");

    if (!is_null_or_empty_string($pro_password) || !is_null_or_empty_string($pro_repassword)):
      if ($pro_password != $pro_repassword):
        array_push($errors, 'Password confirmation did not match');
      endif;
    endif;

    if (sizeof($errors) == 0):

      $user = $em->getRepository('User')->findOneBy(['email' => lower_and_trim($pro_email)]);

      if (isset($user)):
        array_push($errors, 'Username / Email Already exists !');

      else:

        $user = new User;
        $user->setEmail(lower_and_trim($pro_email));
        $user->setFullName(trim($pro_full_name));
        $user->setIsClient($is_client);
        $user->setIsIndividual($is_individual);
        $user->setPassword(trim($pro_password));
        $em->persist($user);
        $em->flush();

        createProfile($user, $em);

        $success_messages = [];
        array_push($success_messages, 'Account has been created!');

        unset($pro_email);
        unset($pro_full_name);
        unset($is_client);
        unset($is_individual);
        unset($pro_password);
        unset($pro_repassword);

      endif;

    endif;
  endif;

## Client Registration

  if (isset($_POST['register-client-form-submit'])):

    $errors            = [];
    $is_client         = $_POST['is-client'];
    $client_full_name  = $_POST['full-name'];
    $client_email      = $_POST['email'];
    $is_individual     = $_POST['is-individual'];
    $client_password   = $_POST['password'];
    $client_repassword = $_POST['repassword'];


    $errors = validation_check($client_full_name,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 3,
       "MAX_CHAR"      => 50,
       "ONLY_CHAR"     => true],
      $errors, "Full Name or Company Name");

    $errors = validation_check($client_email,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 3,
       "MAX_CHAR"      => 50,
       "VALID_EMAIL"   => true],
      $errors, "Email");

    $errors = validation_check($client_password,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 3,
       "MAX_CHAR"      => 50],
      $errors, "Email");

    $errors = validation_check($client_repassword,
      ["NULL_OR_EMPTY" => false],
      $errors, "Email");

    if (!is_null_or_empty_string($client_password) && !is_null_or_empty_string($client_repassword)):
      if ($client_password != $client_repassword):
        array_push($errors, 'Password confirmation did not match');
      endif;
    endif;

    if (sizeof($errors) == 0):

      $user = $em->getRepository('User')->findOneBy(['email' => lower_and_trim($client_email)]);

      if (isset($user)):
        array_push($errors, 'Username / Email Already exists !');
      else:
        $user = new User;
        $user->setEmail(lower_and_trim($client_email));
        $user->setFullName(trim($client_full_name));
        $user->setIsClient($is_client);
        $user->setIsIndividual($is_individual);
        $user->setPassword(trim($client_password));
        $em->persist($user);
        $em->flush();

        createProfile($user, $em);

        $success_messages = [];
        array_push($success_messages, 'Account has been created!');
        unset($client_email);
        unset($client_full_name);
        unset($is_client);
        unset($is_individual);
        unset($client_password);
        unset($client_repassword);
      endif;
    endif;
  endif;