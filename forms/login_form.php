<?php

  if (isset($_POST['login-form-submit'])):

    require_once ROOT_PATH . '/helpers/general_func.php';
    require_once ROOT_PATH . '/models/entities_manager.php';

    $errors   = [];
    $email    = $_POST['username'];
    $password = $_POST['password'];

    $errors = validation_check($email,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 3,
       "MAX_CHAR"      => 50,
       "VALID_EMAIL"   => true],
      $errors, "Email");

    $errors = validation_check($password,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 3,
       "MAX_CHAR"      => 50],
      $errors, "Email");

    if (sizeof($errors) == 0):

      $user = $em->getRepository('User')->findOneBy(['email' => lower_and_trim($email)]);

      if (isset($user) && $user != null):
        if ($user->getPassword() == crypt($password, $user->getSalt())):
          $_SESSION["user_id"] = $user->getId();
          header('Location: /');
          die();
        else:
          array_push($errors, 'Username / Email or password is invalid !');
        endif;
      else:
        array_push($errors, 'Username / Email not exists !');
      endif;
    endif;
  endif;
