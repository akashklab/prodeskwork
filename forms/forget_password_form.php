<?php

require_once ROOT_PATH . '/helpers/general_func.php';
require_once ENTITIES_MANAGER_PATH;

if (isset($_POST['register-form-submit'])):

  $errors = [];
  $first_name = $_POST['register-form-first-name'];
  $last_name = $_POST['register-form-last-name'];
  $email = $_POST['register-form-email'];
  $password = $_POST['register-form-password'];
  $repassword = $_POST['register-form-repassword'];

  if (is_null_or_empty_string($first_name)):
    array_push($errors, 'First Name can not be blank.');
  elseif (length_is_greater_than($first_name, 50)):
    array_push($errors, 'First Name can not max than 50 characters ');
  elseif (length_is_less_than($first_name, 3)):
    array_push($errors, 'First Name can not less than 3 characters ');
  elseif (has_special_chars($first_name)):
    array_push($errors, 'First Name can not contains Special characters ');
  endif;

  if (is_null_or_empty_string($last_name)):
    array_push($errors, 'Last Name can not be blank.');
  elseif (length_is_greater_than($last_name, 50)):
    array_push($errors, 'Last Name can not max than 50 characters ');
  elseif (length_is_less_than($last_name, 3)):
    array_push($errors, 'Last Name can not less than 3 characters ');
  elseif (has_special_chars($last_name, 3)):
    array_push($errors, 'Last Name can not contains Special characters ');
  endif;

  if (is_null_or_empty_string($email)):
    array_push($errors, 'Email can not be blank.');
  elseif (!is_valid_email($email)):
    array_push($errors, 'Email is not valid.');
  elseif (length_is_greater_than($email, 50)):
    array_push($errors, 'Email can not max than 50 characters ');
  endif;

  if (is_null_or_empty_string($password)):
    array_push($errors, 'Password can not be blank.');
  elseif (is_null_or_empty_string($repassword)):
    array_push($errors, 'Re-password can not be blank.');
  elseif (length_is_greater_than($password, 50)):
    array_push($errors, 'Password can not max than 50 characters ');
  elseif (length_is_less_than($password, 5)):
    array_push($errors, 'Password can not less than 5 characters ');
  endif;

  if (!is_null_or_empty_string($password) && !is_null_or_empty_string($repassword)):
    if ($password != $repassword):
      array_push($errors, 'Password confirmation did not match');
    endif;
  endif;

  if (sizeof($errors) == 0):

    $user = $em->getRepository('User')->findOneBy(['email' => $email]);

    if (isset($user)):
      array_push($errors, 'Username / Email Already exists !');
    else:
      $user = new User;
      $user->setEmail($email);
      $user->setPassword($password);
      $em->persist($user);
      $em->flush();
      $success_messages = [];
      array_push($success_messages, 'Account created!');
    endif;
  endif;
endif;