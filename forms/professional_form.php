<?php

  require_once ROOT_PATH . '/helpers/general_func.php';
  require_once ENTITIES_MANAGER_PATH;
  require_once ROOT_PATH . '/helpers/require_all_helper.php';

  ## Basic Info

  if (isset($_POST['pro-basic-info-submit'])):

    $errors           = [];
    $full_name        = $_POST['full-name'];
    $date_of_birth    = $_POST['date-of-birth'];
    $address          = $_POST['address'];
    $city             = $_POST['city'];
    $state            = $_POST['state'];
    $country          = $_POST['country'];
    $mobile_with_code = $_POST['mobile-with-code'];

    $errors = validation_check($full_name, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($date_of_birth, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "DATE" => true], $errors, "Date of Birth");
    $errors = validation_check($address, ["NULL_OR_EMPTY" => false, "MIN_CHAR" => 10, "MAX_CHAR" => 100], $errors, "Address");
    $errors = validation_check($city, ["NULL_OR_EMPTY" => false, "MIN_CHAR" => 2, "MAX_CHAR" => 40, "ONLY_CHAR" => true], $errors, "City");
    $errors = validation_check($state, ["NULL_OR_EMPTY" => false, "MIN_CHAR" => 3, "MAX_CHAR" => 50, "ONLY_CHAR" => true], $errors, "State");
    $errors = validation_check($country, ["NULL_OR_EMPTY" => false, "COUNTRY" => true], $errors, "Country");
    $errors = validation_check($mobile_with_code, ["NULL_OR_EMPTY" => false], $errors, "Mobile with code");

  endif;

  ## Professional Info

  if (isset($_POST['pro-professional-info-form-submit'])):

    $errors                  = [];
    $title                   = $_POST['title'];
    $description             = $_POST['description'];
    $skills                  = $_POST['skills'];
    $high_school_title       = $_POST['high-school-title'];
    $high_school_description = $_POST['high-school-description'];
    $graduation_title        = $_POST['graduation-title'];
    $graduation_description  = $_POST['graduation-description'];

    $errors = validation_check($title, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($description, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($skills, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($high_school_title, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($high_school_description, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($graduation_title, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");
    $errors = validation_check($graduation_description, ["NULL_OR_EMPTY" => false, "MAX_CHAR" => 30, "ONLY_CHAR" => true], $errors, "Full Name");


  endif;


  ## Professional Picture

  if (isset($_POST['pro-professional-picture-form-submit'])):

    $errors          = [];
    $profile_picture = $_POST['profile-picture'];

    ## TODO

  endif;