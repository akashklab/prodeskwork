<?php

  require_once ROOT_PATH . '/helpers/general_func.php';
  require_once ENTITIES_MANAGER_PATH;
  require_once ROOT_PATH . '/helpers/require_all_helper.php';

  ## Pro Registration

  if (isset($_POST['skill-request-submit'])):

    $errors        = [];
    $success       = [];
    $skill_name    = $_POST['skill_name'];
    $description   = $_POST['description'];
    $wikipedia_url = $_POST['wikipedia_url'];

    $errors = validation_check($skill_name,
      ["NULL_OR_EMPTY" => false,
       "MAX_CHAR"      => 30,
       "ONLY_CHAR"     => true],
      $errors, "Skill");

    $errors = validation_check($description,
      ["NULL_OR_EMPTY" => false,
       "MIN_CHAR"      => 100,
       "MAX_CHAR"      => 1000],
      $errors, "Description");

    $errors = validation_check($wikipedia_url,
      ["NULL_OR_EMPTY" => false,
       "URL"           => true],
      $errors, "Wikipedia Url");

    if (sizeof($errors) == 0):

      $skill = $em->getRepository('Skill')->findOneBy(['name' => $skill_name]);

      if (!isset($skill) || $skill == null):
        $skill = new Skill;
        $skill->setName($skill_name);
        $skill->setWikipediaUrl($wikipedia_url);
        $skill->setDescription($description);
        $em->persist($skill);
        $em->flush();
        array_push($success, "Successfully done, We will review <strong>$skill->getName()</strong> and add in system");
      else:
        array_push($success, "Successfully done, We will review <strong>$skill->getName()</strong> and add in system");
      endif;
    endif;
  endif;
