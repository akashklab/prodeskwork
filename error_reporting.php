<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

function shutDownFunction()
{
    $error = error_get_last();
    // fatal error, E_ERROR === 1
    if ($error['type'] === E_ERROR) {
        header("Location: /500.html");
        die();
    }
}

register_shutdown_function('shutDownFunction');