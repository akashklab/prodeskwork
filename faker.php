<?php
  require_once 'config.php';
  require_once 'vendor/fzaninotto/faker/src/autoload.php';
  require_once ROOT_PATH . '/helpers/general_func.php';
  require_once ENTITIES_MANAGER_PATH;
  require_once ROOT_PATH . '/helpers/require_all_helper.php';

  $faker = Faker\Factory::create();


  foreach ($em->getRepository('User')->findAll() as $object):
    $em->remove($object);
    $em->flush();
  endforeach;

  foreach ($em->getRepository("Professional")->findAll() as $object):
    $em->remove($object);
    $em->flush();
  endforeach;

  foreach ($em->getRepository("CompanyProfessional")->findAll() as $object):
    $em->remove($object);
    $em->flush();
  endforeach;

  foreach ($em->getRepository("Skill")->findAll() as $object):
    $em->remove($object);
    $em->flush();
  endforeach;

  ## Create User email
  for ($i = 0; $i <= 100; $i++):

    $email = $faker->email;
    $user  = $em->getRepository('User')->findOneBy(['email' => lower_and_trim($email)]);

    if (!isset($user)):
      $user = new User;
      $user->setEmail(lower_and_trim($email));
      $user->setFullName(trim($faker->name));
      $user->setIsClient(0);
      $user->setIsIndividual(rand(0, 1));
      $user->setPassword(trim("password"));
      $em->persist($user);
      $em->flush();
      createProfile($user, $em);
    endif;
  endfor;

  ## Create Skills
  for ($i = 0; $i <= 100; $i++):

    $skill_name = $faker->word;
    $skill      = $em->getRepository('Skill')->findOneBy(['name' => $skill_name]);

    if (!isset($skill)):
      $skill = new Skill;
      $skill->setName($skill_name);
      $skill->setDescription($faker->paragraph);
      $skill->setWikipediaUrl($faker->url);
      $em->persist($skill);
      $em->flush();
    endif;

  endfor;

  ## Create jobs
  for ($i = 0; $i <= 100; $i++):

    $title = $faker->paragraph;
    $description = $faker->paragraphs();

      $job = new Job;
      $job->setTitle($title);
      $job->setDescription($description);
      $em->persist($job);
      $em->flush();

  endfor;
