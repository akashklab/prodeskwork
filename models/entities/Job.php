<?php

/**
 * @Entity @HasLifecycleCallbacks
 * @Table(name="jobs")
 **/
Class Job
{

  /** @Id @Column(type="integer") @GeneratedValue * */
  protected $id;

  /**
   * @Column(type="integer", name="client_id")
   * @var integer
   */
  protected $client_id;

  /**
   * @Column(type="string", name="title")
   * @var string
   */
  protected $title;

  /**
   * @Column(type="text", name="description")
   * @var string
   */
  protected $description;

  /**
   * @Column(type="string", name="skills")
   * @var string
   */
  protected $skills;

  /**
   * @Column(type="string", name="created_at")
   */
  protected $created_at;

  /**
   * @Column(type="string", name="updated_at")
   */
  protected $updated_at;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getClientId()
  {
    return $this->client_id;
  }

  /**
   * @param int $client_id
   */
  public function setClientId($client_id)
  {
    $this->client_id = $client_id;
  }

  /**
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @param string $title
   */
  public function setTitle($title)
  {
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getSkills()
  {
    return $this->skills;
  }

  /**
   * @param string $skills
   */
  public function setSkills($skills)
  {
    $this->skills = $skills;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param mixed $created_at
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
  }

  /**
   * @return mixed
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param mixed $updated_at
   */
  public function setUpdatedAt($updated_at)
  {
    $this->updated_at = $updated_at;
  }

  /** @PrePersist */
  public function prePersist()
  {
    $this->created_at = date('Y-m-d H:i:s');
    $this->updated_at = date('Y-m-d H:i:s');
  }

}
