<?php

/**
 * @Entity @HasLifecycleCallbacks
 * @Table(name="jobs_applied")
 **/
Class JobApplied
{

  /** @Id @Column(type="integer") @GeneratedValue * */
  protected $id;

  /**
   * @Column(type="integer", name="job_id")
   * @var integer
   */
  protected $job_id;

  /**
   * @Column(type="integer", name="user_id")
   * @var integer
   */
  protected $user_id;

  /**
   * @Column(type="integer", name="applied_by_user")
   * @var boolean
   */
  protected $applied_by_user;

  /**
   * @Column(type="string", name="message_by_user")
   * @var string
   */
  protected $message_by_user;

  /**
   * @Column(type="string", name="message_by_client")
   * @var string
   */
  protected $message_by_client;

  /**
   * @Column(type="integer", name="cancelled")
   * @var boolean
   */
  protected $cancelled;

  /**
   * @Column(type="string", name="cancelled_reason")
   * @var string
   */
  protected $cancelled_reason;

  /**
   * @Column(type="string", name="created_at")
   */
  protected $created_at;

  /**
   * @Column(type="string", name="updated_at")
   */
  protected $updated_at;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return int
   */
  public function getJobId()
  {
    return $this->job_id;
  }

  /**
   * @param int $job_id
   */
  public function setJobId($job_id)
  {
    $this->job_id = $job_id;
  }

  /**
   * @return int
   */
  public function getUserId()
  {
    return $this->user_id;
  }

  /**
   * @param int $user_id
   */
  public function setUserId($user_id)
  {
    $this->user_id = $user_id;
  }

  /**
   * @return bool
   */
  public function isAppliedByUser()
  {
    return $this->applied_by_user;
  }

  /**
   * @param bool $applied_by_user
   */
  public function setAppliedByUser($applied_by_user)
  {
    $this->applied_by_user = $applied_by_user;
  }

  /**
   * @return string
   */
  public function getMessageByUser()
  {
    return $this->message_by_user;
  }

  /**
   * @param string $message_by_user
   */
  public function setMessageByUser($message_by_user)
  {
    $this->message_by_user = $message_by_user;
  }

  /**
   * @return string
   */
  public function getMessageByClient()
  {
    return $this->message_by_client;
  }

  /**
   * @param string $message_by_client
   */
  public function setMessageByClient($message_by_client)
  {
    $this->message_by_client = $message_by_client;
  }

  /**
   * @return bool
   */
  public function isCancelled()
  {
    return $this->cancelled;
  }

  /**
   * @param bool $cancelled
   */
  public function setCancelled($cancelled)
  {
    $this->cancelled = $cancelled;
  }

  /**
   * @return string
   */
  public function getCancelledReason()
  {
    return $this->cancelled_reason;
  }

  /**
   * @param string $cancelled_reason
   */
  public function setCancelledReason($cancelled_reason)
  {
    $this->cancelled_reason = $cancelled_reason;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param mixed $created_at
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
  }

  /**
   * @return mixed
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param mixed $updated_at
   */
  public function setUpdatedAt($updated_at)
  {
    $this->updated_at = $updated_at;
  }

  /** @PrePersist */
  public function prePersist()
  {
    $this->created_at = date('Y-m-d H:i:s');
    $this->updated_at = date('Y-m-d H:i:s');
  }

}
