<?php

  /**
   * @Entity @HasLifecycleCallbacks
   * @Table(name="professionals")
   **/
  Class Professional
  {

    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;

    /**
     * @Column(type="integer", name="user_id")
     * @var integer
     */
    protected $user_id;

    /**
     * @Column(type="date", name="date_of_birth")
     * @var string
     */
    protected $date_of_birth;

    /**
     * @Column(type="string", name="address")
     * @var string
     */
    protected $address;

    /**
     * @Column(type="string", name="city")
     * @var string
     */
    protected $city;

    /**
     * @Column(type="string", name="state")
     * @var string
     */
    protected $state;

    /**
     * @Column(type="string", name="country")
     * @var string
     */
    protected $country;

    /**
     * @Column(type="string", name="mobile_with_code")
     * @var string
     */
    protected $mobile_with_code;

    /**
     * @Column(type="string", name="phone_with_code")
     * @var string
     */
    protected $phone_with_code;

    /**
     * @Column(type="string", name="title")
     * @var string
     */
    protected $title;

    /**
     * @Column(type="string", name="description")
     * @var string
     */
    protected $description;

    /**
     * @Column(type="string", name="skills")
     * @var string
     */
    protected $skills;

    /**
     * @Column(type="string", name="high_school_title")
     * @var string
     */
    protected $high_school_title;

    /**
     * @Column(type="string", name="high_school_description")
     * @var string
     */
    protected $high_school_description;

    /**
     * @Column(type="string", name="graduation_title")
     * @var string
     */
    protected $graduation_title;

    /**
     * @Column(type="string", name="graduation_description")
     * @var string
     */
    protected $graduation_description;

    /**
     * @Column(type="string", name="picture")
     * @var string
     */
    protected $picture;

    /**
     * @Column(type="string", name="picture_uploaded_at")
     */
    protected $picture_uploaded_at;

    /**
     * @Column(type="integer", name="picture_uploaded_count")
     * @var integer
     */
    protected $picture_uploaded_count;

    /**
     * @Column(type="string", name="created_at")
     */
    protected $created_at;

    /**
     * @Column(type="string", name="updated_at")
     */
    protected $updated_at;

    /**
     * @Column(type="integer", name="updated_information_count")
     * @var integer
     */
    protected $updated_information_count;

    /**
     * @return mixed
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
      $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
      return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
      $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getDateOfBirth()
    {
      return $this->date_of_birth;
    }

    /**
     * @param string $date_of_birth
     */
    public function setDateOfBirth($date_of_birth)
    {
      $this->date_of_birth = $date_of_birth;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
      return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
      $this->address = $address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
      $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
      return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
      $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
      $this->country = $country;
    }

    /**
     * @return string
     */
    public function getMobileWithCode()
    {
      return $this->mobile_with_code;
    }

    /**
     * @param string $mobile_with_code
     */
    public function setMobileWithCode($mobile_with_code)
    {
      $this->mobile_with_code = $mobile_with_code;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
      return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
      $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
      $this->description = $description;
    }

    /**
     * @return string
     */
    public function getSkills()
    {
      return $this->skills;
    }

    /**
     * @param string $skills
     */
    public function setSkills($skills)
    {
      $this->skills = $skills;
    }

    /**
     * @return string
     */
    public function getHighSchoolTitle()
    {
      return $this->high_school_title;
    }

    /**
     * @param string $high_school_title
     */
    public function setHighSchoolTitle($high_school_title)
    {
      $this->high_school_title = $high_school_title;
    }

    /**
     * @return string
     */
    public function getHighSchoolDescription()
    {
      return $this->high_school_description;
    }

    /**
     * @param string $high_school_description
     */
    public function setHighSchoolDescription($high_school_description)
    {
      $this->high_school_description = $high_school_description;
    }

    /**
     * @return string
     */
    public function getGraduationTitle()
    {
      return $this->graduation_title;
    }

    /**
     * @param string $graduation_title
     */
    public function setGraduationTitle($graduation_title)
    {
      $this->graduation_title = $graduation_title;
    }

    /**
     * @return string
     */
    public function getGraduationDescription()
    {
      return $this->graduation_description;
    }

    /**
     * @param string $graduation_description
     */
    public function setGraduationDescription($graduation_description)
    {
      $this->graduation_description = $graduation_description;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
      return $this->created_at;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt($created_at)
    {
      $this->created_at = $created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
      return $this->updated_at;
    }

    /**
     * @param string $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
      $this->updated_at = $updated_at;
    }

    /** @PrePersist */
    public function prePersist()
    {
      $this->created_at = date('Y-m-d H:i:s');
      $this->updated_at = date('Y-m-d H:i:s');
    }

    /** @PreUpdate */
    public function preUpdate()
    {
      $this->updated_at = date('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function getPhoneWithCode()
    {
      return $this->phone_with_code;
    }

    /**
     * @param string $phone_with_code
     */
    public function setPhoneWithCode($phone_with_code)
    {
      $this->phone_with_code = $phone_with_code;
    }

    /**
     * @return string
     */
    public function getPicture()
    {
      return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture($picture)
    {
      $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getPictureUploadedAt()
    {
      return $this->picture_uploaded_at;
    }

    /**
     * @param mixed $picture_uploaded_at
     */
    public function setPictureUploadedAt($picture_uploaded_at)
    {
      $this->picture_uploaded_at = $picture_uploaded_at;
    }

    /**
     * @return int
     */
    public function getPictureUploadedCount()
    {
      return $this->picture_uploaded_count;
    }

    /**
     * @param int $picture_uploaded_count
     */
    public function setPictureUploadedCount($picture_uploaded_count)
    {
      $this->picture_uploaded_count = $picture_uploaded_count;
    }

    /**
     * @return int
     */
    public function getUpdatedInformationCount()
    {
      return $this->updated_information_count;
    }

    /**
     * @param int $updated_information_count
     */
    public function setUpdatedInformationCount($updated_information_count)
    {
      $this->updated_information_count = $updated_information_count;
    }


  }
