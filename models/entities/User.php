<?php

/**
 * @Entity @HasLifecycleCallbacks
 * @Table(name="users")
 **/
Class User
{

  /** @Id @Column(type="integer") @GeneratedValue * */
  protected $id;

  /**
   * @Column(type="integer", name="is_client")
   * @var boolean
   */
  protected $is_client;

  /**
   * @Column(type="integer", name="is_individual")
   * @var boolean
   */
  protected $is_individual;

  /**
   * @Column(type="string", name="email")
   * @var string
   */
  protected $email;

  /**
   * @Column(type="string", name="full_name")
   * @var string
   */
  protected $full_name;

  /**
   * @Column(type="string", name="uuid")
   * @var string
   */
  protected $uuid;

  /**
   * @Column(type="string", name="username")
   * @var string
   */
  protected $username;

  /**
   * @Column(type="string", name="security_question")
   * @var string
   */
  protected $security_question;

  /**
   * @Column(type="string", name="security_answer")
   * @var string
   */
  protected $security_answer;

  /**
   * @Column(type="string", name="salt")
   * @var string
   */
  protected $salt;

  /**
   * @Column(type="string", name="password")
   * @var string
   */
  protected $password;

  /**
   * @Column(type="string", name="created_at")
   */
  protected $created_at;

  /**
   * @Column(type="boolean", name="term_and_condition")
   * @var integer
   */
  protected $term_and_condition;

  /**
   * @Column(type="string", name="updated_at")
   */
  protected $updated_at;

  /** @PrePersist */
  public function prePersist()
  {
    $this->created_at = date('Y-m-d H:i:s');
    $this->updated_at = date('Y-m-d H:i:s');
    $this->generate_salt();
    $this->encrypt_password();
  }

  /** @PostPersist */
  public function postPersist()
  {
    ## TODO;
  }

  public function generate_salt()
  {
    $charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $randStringLen = 64;
    $randString = "";
    for ($i = 0; $i < $randStringLen; $i++) {
      $randString .= $charset[mt_rand(0, strlen($charset) - 1)];
    }
    $this->setSalt($randString);
  }

  public function encrypt_password()
  {
    $this->setPassword(crypt($this->getPassword(), $this->getSalt()));
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return boolean
   */
  public function getIsClient()
  {
    return $this->is_client;
  }

  /**
   * @param int $is_client
   */
  public function setIsClient($is_client)
  {
    $this->is_client = $is_client;
  }

  /**
   * @return boolean
   */
  public function getIsIndividual()
  {
    return $this->is_individual;
  }

  /**
   * @param int $is_individual
   */
  public function setIsIndividual($is_individual)
  {
    $this->is_individual = $is_individual;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * @return string
   */
  public function getFullName()
  {
    return $this->full_name;
  }

  /**
   * @param string $full_name
   */
  public function setFullName($full_name)
  {
    $this->full_name = $full_name;
  }

  /**
   * @return string
   */
  public function getUuid()
  {
    return $this->uuid;
  }

  /**
   * @param string $uuid
   */
  public function setUuid($uuid)
  {
    $this->uuid = $uuid;
  }

  /**
   * @return string
   */
  public function getUsername()
  {
    return $this->username;
  }

  /**
   * @param string $username
   */
  public function setUsername($username)
  {
    $this->username = $username;
  }

  /**
   * @return string
   */
  public function getSecurityQuestion()
  {
    return $this->security_question;
  }

  /**
   * @param string $security_question
   */
  public function setSecurityQuestion($security_question)
  {
    $this->security_question = $security_question;
  }

  /**
   * @return string
   */
  public function getSecurityAnswer()
  {
    return $this->security_answer;
  }

  /**
   * @param string $security_answer
   */
  public function setSecurityAnswer($security_answer)
  {
    $this->security_answer = $security_answer;
  }

  /**
   * @return string
   */
  public function getSalt()
  {
    return $this->salt;
  }

  /**
   * @param string $salt
   */
  public function setSalt($salt)
  {
    $this->salt = $salt;
  }

  /**
   * @return string
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @param string $password
   */
  public function setPassword($password)
  {
    $this->password = $password;
  }

  /**
   * @return mixed
   */
  public function getTermAndCondition()
  {
    return $this->term_and_condition;
  }

  /**
   * @param mixed $term_and_condition
   */
  public function setTermAndCondition($term_and_condition)
  {
    $this->term_and_condition = $term_and_condition;
  }

  /**
   * @return mixed
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param mixed $created_at
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
  }

  /**
   * @return mixed
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param mixed $updated_at
   */
  public function setUpdatedAt($updated_at)
  {
    $this->updated_at = $updated_at;
  }

}
