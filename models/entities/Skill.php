<?php

/**
 * @Entity @HasLifecycleCallbacks
 * @Table(name="skills")
 **/
Class Skill
{

  /** @Id @Column(type="integer") @GeneratedValue * */
  protected $id;

  /**
   * @Column(type="string", name="name")
   * @var string
   */
  protected $name;

  /**
   * @Column(type="text", name="description")
   * @var string
   */
  protected $description;

  /**
   * @Column(type="string", name="wikipedia_url")
   * @var string
   */
  protected $wikipedia_url;

  /**
   * @Column(type="integer", name="approved")
   * @var boolean
   */
  protected $approved;

  /**
   * @Column(type="string", name="created_at")
   */
  protected $created_at;

  /**
   * @Column(type="string", name="updated_at")
   */
  protected $updated_at;

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param mixed $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * @return string
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param string $created_at
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
  }

  /**
   * @return string
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param string $updated_at
   */
  public function setUpdatedAt($updated_at)
  {
    $this->updated_at = $updated_at;
  }

  /**
   * @return string
   */
  public function getWikipediaUrl()
  {
    return $this->wikipedia_url;
  }

  /**
   * @param string $wikipedia_url
   */
  public function setWikipediaUrl($wikipedia_url)
  {
    $this->wikipedia_url = $wikipedia_url;
  }

  /** @PrePersist */
  public function prePersist()
  {
    $this->created_at = date('Y-m-d H:i:s');
    $this->updated_at = date('Y-m-d H:i:s');
  }

}
