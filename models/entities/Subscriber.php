<?php

/**
 * @Entity
 * @Table(name="subscribers")
 **/
Class Subscriber
{

  /** @Id @Column(type="integer") @GeneratedValue * */
  protected $id;

  /**
   * @Column(type="string", name="email")
   * @var string
   */
  protected $email;

  /**
   * @Column(type="boolean", name="enabled")
   * @var boolean
   */
  protected $enabled;

  /**
   * @Column(type="datetime", name="created_at")
   * @var string
   */
  protected $created_at;

  /**
   * @Column(type="datetime", name="updated_at")
   * @var string
   */
  protected $updated_at;


  public function getId()
  {
    return $this->id;
  }


  public function getTypeIs()
  {
    return $this->type_is;
  }

  public function setTypeIs($type_is)
  {
    $this->type_is = $type_is;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

}
