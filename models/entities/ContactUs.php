<?php
  
  /**
   * @Entity @HasLifecycleCallbacks
   * @Table(name="contact_us")
   **/
  Class ContactUs
  {
    
    /** @Id @Column(type="integer") @GeneratedValue * */
    protected $id;
    
    /**
     * @Column(type="string", name="name")
     * @var string
     */
    protected $name;
    
    /**
     * @Column(type="string", name="email")
     * @var string
     */
    protected $email;
    
    /**
     * @Column(type="string", name="reason")
     * @var string
     */
    protected $reason;
    
    /**
     * @Column(type="string", name="message")
     * @var string
     */
    protected $message;
    
    /**
     * @Column(type="string", name="created_at")
     */
    protected $created_at;
    
    /**
     * @Column(type="string", name="updated_at")
     */
    protected $updated_at;
    
    /** @PrePersist */
    public function prePersist()
    {
      $this->created_at = date('Y-m-d H:i:s');
      $this->updated_at = date('Y-m-d H:i:s');
    }
    
    /** @PostPersist */
    public function postPersist()
    {
      ## TODO;
    }
  
    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }
  
    /**
     * @param string $name
     */
    public function setName($name)
    {
      $this->name = $name;
    }
  
    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }
  
    /**
     * @param string $email
     */
    public function setEmail($email)
    {
      $this->email = $email;
    }
  
    /**
     * @return string
     */
    public function getReason()
    {
      return $this->reason;
    }
  
    /**
     * @param string $reason
     */
    public function setReason($reason)
    {
      $this->reason = $reason;
    }
  
    /**
     * @return string
     */
    public function getMessage()
    {
      return $this->message;
    }
  
    /**
     * @param string $message
     */
    public function setMessage($message)
    {
      $this->message = $message;
    }
  
    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
      return $this->created_at;
    }
  
    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
      $this->created_at = $created_at;
    }
  
    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
      return $this->updated_at;
    }
  
    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
      $this->updated_at = $updated_at;
    }
    
  }