<?php
// bootstrap.php
// Include Composer Autoload (relative to project root).
require_once ROOT_PATH."/vendor/autoload.php";


use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

$paths = array(ROOT_PATH."/models/entities");

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => DATABASE_USERNAME,
    'password' => DATABASE_PASSWORD,
    'dbname'   => DATABASE_NAME,
);

$config = Setup::createAnnotationMetadataConfiguration($paths, DEVELOPMENT_MODE);
global $em;
$em = EntityManager::create($dbParams, $config);

require_once ROOT_PATH . '/models/require_all_entities.php';